#ifndef "dungeon.h"
#define "dungeon.h"
#include <cstdlib>
#include <stdio>
#include <iostream>
#include <string>

typedef struct room{
	int tiles[8][12];
	
}room_t;

class Dungeon{
public:
	Dungeon(int seed);
	std::string level_name;
	char level_num[2];
private:
	int rooms[4][4]; //stores the values for premade rooms (0 is random)
	int tiles[8*4][12*4];
	
	
}

#endif
